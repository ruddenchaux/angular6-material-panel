import { PrivateCoreModule } from './private-core.module';

describe('CoreModule', () => {
  let coreModule: PrivateCoreModule;

  beforeEach(() => {
    coreModule = new PrivateCoreModule();
  });

  it('should create an instance', () => {
    expect(coreModule).toBeTruthy();
  });
});
