import { PrivateSharedModule } from './private-shared.module';

describe('SharedModule', () => {
  let sharedModule: PrivateSharedModule;

  beforeEach(() => {
    sharedModule = new PrivateSharedModule();
  });

  it('should create an instance', () => {
    expect(sharedModule).toBeTruthy();
  });
});
