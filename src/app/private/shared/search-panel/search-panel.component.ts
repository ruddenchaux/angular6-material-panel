import { Component, OnInit, Input } from '@angular/core';


export interface SearchField {
  name: string;
  model: string;
  label: string;
}

@Component({
  selector: 'app-search-panel',
  templateUrl: './search-panel.component.html',
  styleUrls: ['./search-panel.component.css']
})
export class SearchPanelComponent implements OnInit {
  @Input('title') public title: string;
  @Input('description') public description: string;
  @Input('fields') public fields: SearchField[];

  public panelOpenState = false;

  constructor() { }

  ngOnInit() {}

}
