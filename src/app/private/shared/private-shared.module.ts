import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {MatExpansionModule} from '@angular/material/expansion';
import {MatInputModule} from '@angular/material/input';

import { SearchPanelComponent } from '@app/private/shared/search-panel/search-panel.component';
import { SharedModule } from '@app/shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    MatExpansionModule,
    MatInputModule
  ],
  declarations: [SearchPanelComponent],
  exports: [
    SearchPanelComponent
  ]
})
export class PrivateSharedModule { }
