import { Component, OnInit, OnDestroy } from '@angular/core';
import { BreakpointObserver, Breakpoints, BreakpointState } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map, filter, mergeMap } from 'rxjs/operators';
import { ActivatedRoute, Router, RoutesRecognized, NavigationEnd } from '@angular/router';

@Component({
    selector: 'app-private-layout',
    templateUrl: './private.component.html',
    styleUrls: ['./private.component.scss']
})
export class PrivateComponent implements OnInit, OnDestroy {

    isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
        map(result => result.matches)
    );
    routerSub$: any;
    pageName: string;

    constructor(
        private breakpointObserver: BreakpointObserver,
        private activatedRoute: ActivatedRoute,
        private router: Router
    ) {}

    ngOnInit() {
        this.routerSub$ = this.router.events
            .pipe(
                filter(event => event instanceof NavigationEnd),
                map(() => this.activatedRoute),
                map(route => {
                    while (route.firstChild) { route = route.firstChild; }
                    return route;
                }),
                filter(route => route.outlet === 'primary'),
                mergeMap(route => route.data)
            )
            .subscribe((data) => {
                this.pageName = data.titleLabel;
            });
    }

    ngOnDestroy(): void {
        this.routerSub$.unsubscribe();
    }

}
