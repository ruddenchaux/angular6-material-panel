import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PrivateComponent } from '@app/private/main/private.component';
import { DashboardComponent } from '@app/private/dashboard/dashboard.component';
import { ActivityComponent } from '@app/private/activity/activity.component';
import { AuthGuard } from '@app/core/services/auth/auth-guard.service';

const routes: Routes = [
  {
    path: '',
    component: PrivateComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: '',
        canActivateChild: [AuthGuard],
        children: [
          {
            path: '',
            component: DashboardComponent,
            data: {
              titleLabel: 'APP_DASHBOARD'
            }
          },
          {
            path: 'activities',
            component: ActivityComponent,
            data: {
              titleLabel: 'ACTIVITY.TITLE'
            }
          }
        ]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PrivateRoutingModule { }
