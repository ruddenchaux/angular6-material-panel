import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PrivateRoutingModule } from '@app/private/main/private-routing.module';
import { DashboardComponent } from '@app/private/dashboard/dashboard.component';
import { ActivityComponent } from '@app/private/activity/activity.component';
import { PrivateCoreModule } from '@app/private/core/private-core.module';
import { PrivateComponent } from '@app/private/main/private.component';
import { SharedModule } from '@app/shared/shared.module';
import { PrivateSharedModule } from '@app/private/shared/private-shared.module';

@NgModule({
  imports: [
    CommonModule,
    PrivateCoreModule,
    PrivateRoutingModule,
    SharedModule,
    PrivateSharedModule
  ],
  declarations: [
    PrivateComponent,
    DashboardComponent,
    ActivityComponent,
  ]
})
export class PrivateModule { }
