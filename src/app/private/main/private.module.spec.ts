import { PrivateModule } from '@app/private/main/private.module';

describe('PrivateModule', () => {
  let privateModule: PrivateModule;

  beforeEach(() => {
    privateModule = new PrivateModule();
  });

  it('should create an instance', () => {
    expect(privateModule).toBeTruthy();
  });
});
