import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort } from '@angular/material';
import { TranslateService } from '@ngx-translate/core';

import { ActivityDataSource } from './activity-datasource';
import { SearchField } from '@app/private/shared/search-panel/search-panel.component';

@Component({
  selector: 'app-activity',
  templateUrl: './activity.component.html',
  styleUrls: ['./activity.component.scss']
})
export class ActivityComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  dataSource: ActivityDataSource;

  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ['id', 'name'];

  searchFields: SearchField[];

  constructor(private translate: TranslateService) {}

  ngOnInit() {
    this.dataSource = new ActivityDataSource(this.paginator, this.sort);

    this.translate.get([
      'ACTIVITY.SEARCH.FIELDS.USER',
      'ACTIVITY.SEARCH.FIELDS.DEVICE'
    ]).subscribe((translated: string[]) => {
        this.searchFields = [
          {
            name: 'userId',
            model: 'userId',
            label: translated['ACTIVITY.SEARCH.FIELDS.USER']
          },
          {
            name: 'deviceId',
            model: 'deviceId',
            label: translated['ACTIVITY.SEARCH.FIELDS.DEVICE']
          }
        ];
    });
  }
}
