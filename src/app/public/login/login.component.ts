import { Component, ViewChild, ElementRef, OnInit } from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';
import { FormControl, Validators } from '@angular/forms';

import { TranslateService } from '@ngx-translate/core';

import { AuthService } from '../../core/services/auth/auth.service';
import { MatFormField } from '@angular/material/form-field';
import { MatCheckbox } from '@angular/material/checkbox';


@Component({
  styleUrls: ['./login.component.scss'],
  templateUrl: './login.component.html'
})
export class LoginComponent implements OnInit {
  email = new FormControl('', [Validators.required, Validators.email]);
  hide: boolean;
  pwdLabel: string;
  emailLabel: string;
  checkboxLabel: string;
  @ViewChild('pwdEl') pwdEl: MatFormField;
  @ViewChild('emailEl') emailEl: MatFormField;
  @ViewChild('chekboxEl') chekboxEl: MatCheckbox;

  constructor(
    public authService: AuthService, 
    public router: Router, 
    private translate: TranslateService
  ) {}

  ngDoCheck() {
    this.refreshOutlineGap();
  }
  ngOnInit() {
    this.translate.get([
      'LOGIN.PASSWORD',
      'LOGIN.EMAIL',
      'LOGIN.REMEMBERME'
    ]).subscribe((translated: string[]) => {
        this.pwdLabel = translated['LOGIN.PASSWORD'];
        this.emailLabel = translated['LOGIN.EMAIL'];
        this.refreshOutlineGap();
        this.checkboxLabel = translated['LOGIN.REMEMBERME'];
        
        // workarround for UI glitch bug checkbox
        this.chekboxEl.toggle();
    });
  }

  private refreshOutlineGap() {
    this.pwdEl.updateOutlineGap();
    this.emailEl.updateOutlineGap();
  }

  login() {

    this.authService.login().subscribe(() => {
      if (this.authService.isLoggedIn) {
        // Get the redirect URL from our auth service
        // If no redirect has been set, use the default
        const redirect = this.authService.redirectUrl ? this.authService.redirectUrl : '/';

        // Set our navigation extras object
        // that passes on our global query params and fragment
        const navigationExtras: NavigationExtras = {
          queryParamsHandling: 'preserve',
          preserveFragment: true
        };

        // Redirect the user
        this.router.navigate([redirect], navigationExtras);
      }
    });
  }

  logout() {
    this.authService.logout();
  }

  getErrorMessage() {
    return this.email.hasError('required') ? 'You must enter a value' :
        this.email.hasError('email') ? 'Not a valid email' :
            '';
  }
}
