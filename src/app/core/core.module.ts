import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PageNotFoundComponent } from '@app/core/component/not-found.component';
import { UserService } from '@app/core/services/user/user.service';
import { AuthService } from '@app/core/services/auth/auth.service';
import { AuthGuard } from '@app/core/services/auth/auth-guard.service';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    PageNotFoundComponent
  ],
  exports: [
    PageNotFoundComponent
  ],
  providers: [
    UserService,
    AuthService,
    AuthGuard
  ]
})
export class CoreModule { }
