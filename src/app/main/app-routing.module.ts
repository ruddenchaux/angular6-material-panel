import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '@app/core/services/auth/auth-guard.service';
import { PageNotFoundComponent } from '../core/component/not-found.component';
import { CanDeactivateGuard } from '../core/services/can-deactivate-guard.service';
import { SelectivePreloadingStrategy } from '../core/services/selective-preload-staretegy';


const routes: Routes = [
  {
    path: '',
    loadChildren: '../private/main/private.module#PrivateModule',
    canLoad: [AuthGuard]
  },
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  imports: [
    RouterModule.forRoot(
      routes,
      {
        enableTracing: true, // <-- debugging purposes only
        preloadingStrategy: SelectivePreloadingStrategy,
        /* useHash: true */
      }
    )
  ],
  exports: [RouterModule],
  providers: [
    CanDeactivateGuard,
    SelectivePreloadingStrategy
  ]
})
export class AppRoutingModule { }
